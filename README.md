# Hidden Search Widget

This is a search widget that's toggleable, but not exactly functional.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
